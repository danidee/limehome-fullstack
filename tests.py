import unittest

from fastapi.testclient import TestClient

from playhouse.db_url import connect

from main import app
from api.models import Booking


# use an in-memory database for tests.
test_db = connect('sqlite:///:memory:')
models = [Booking]


class BaseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Bind model classes to test db
        test_db.bind(models, bind_refs=False, bind_backrefs=False)

        test_db.connect()
        test_db.create_tables(models)

        cls.client = TestClient(app)
        cls.hotel_id = 'ChIJGw8n1RtOqEcRhMVZgzuKADg'

    def test_create_booking(self):
        data = {
            'hotel_id': self.hotel_id,
            'customer_name': 'James Maddison',
            'phone_number': '68033445123',
            'check_in': '2021-01-21',
            'check_out': '2021-01-29',
            'number_of_guests': 1
        }
        response = self.client.post('/api/1.0/bookings', json=data)

        self.assertEqual(response.status_code, 200)

    def test_list_hotel_bookings(self):
        response = self.client.get(
            f'/api/1.0/hotels/{self.hotel_id}/bookings',
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

    @classmethod
    def tearDownClass(cls):
        test_db.drop_tables(models)
        test_db.close()
