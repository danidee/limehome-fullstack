Vue.component('book', {
  props: {
    place: Object
  },

  data: function () {
    return {
      booked: false,
      bookingData: {
        customerName: '',
        phoneNumber: null,
        checkIn: '',
        checkOut: '',
        numberOfGuests: 1
      }
    }
  },

  methods: {
    handleSubmit: function () {
      const data = {
        'hotel_id': this.place.place_id,
        'customer_name': this.bookingData.customerName,
        'phone_number': this.bookingData.phoneNumber,
        'check_in': this.bookingData.checkIn,
        'check_out': this.bookingData.checkOut,
        'number_of_guests': this.bookingData.numberOfGuests
      }

      axios.post('/api/1.0/bookings', data)
        .then((response) => {
          console.log(response)
          this.booked = true
        })
        .catch((error) => {
          if (error.response.status === 422) {
            const errorString = JSON.stringify(
              error.response.data.detail.map(x => `${x.loc[1]}: ${x.msg}`)
            )
            alert(errorString)
          }
          alert('An error occured')
        })
    }
  },

  template: `
    <transition name="custom-classes-transition" enter-active-class="animated fadeIn faster" leave-active-class="animated fadeOut faster">
      <form v-if="!booked" @submit.prevent="handleSubmit">
        <h3 class="text-center">{{ place.name }}</h3>
        <section>
          <input v-model="bookingData.customerName" name="customername" type="text" placeholder="Full name" required />

          <input v-model="bookingData.phoneNumber" name="phonenumber" type="number" placeholder="Phone number" required />

          <div class="checkin-checkout">
            <div>
              <label for="checkin">Check in</label>
              <input v-model="bookingData.checkIn" name="checkin" type="date" placeholder="Check in" required />
            </div>

            <div>
              <label for="checkout">Check out</label>
              <input v-model="bookingData.checkOut" name="checkout" type="date" placeholder="Check out" required />
            </div>
          </div>

          <label for="numberofguests">Guests</label>
          <input v-model="bookingData.numberOfGuests" name="numberofguests" type="number" placeholder="number of guests" required />

          <input type="submit" value="book" />
        </section>
      </form>

      <div class="booking-success" v-else>
        <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
          <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
          <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
        </svg>
        <br />
        Booking succesful!
      </div>
    </transition>
  `
})