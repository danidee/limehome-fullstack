Vue.component('hotel-detail', {
  props: {
    place: Object,
    index: Number
  },

  template: `
  <div id="hotel-detail">
    <img :src="'https://picsum.photos/id/' + index + '/300/300'" />

      <h3>{{ place.name }}</h3>
      <h3>place id: {{ place.place_id }}</h3>
      <h3 class="price">€{{ place.user_ratings_total }}</h3>
    </div>
  `
})