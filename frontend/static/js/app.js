new Vue({
  el: '#app',

  data: function () {
    return {
      // Map data
      map: undefined,
      bounds: undefined,
      currentPosition: { lat: 52.520008, lng: 13.404954 }, // Berlin
      defaultPlaces: [],
      places: [],

      // Regular data
      booking: false,
      selectedPlace: {},
      showHotelDetails: false,
      placeIndex: null,
    }
  },

  methods: {
    geoLocateUser: function () {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          const pos = { lat: position.coords.latitude, lng: position.coords.longitude }
          this.currentPosition = pos
          this.map = new google.maps.Map(document.getElementById('map'), {
            center: pos,
            zoom: 10
          })
          this.bounds.extend(pos)
          this.map.setCenter(pos)
          this.getNearbyPlaces(pos)
        }, () => {
          // Browser supports geolocation, but user has denied permission
          this.getNearbyPlaces(this.currentPosition)
        })
      } else {
        // Browser doesn't support geolocation
        this.getNearbyPlaces(this.currentPosition)
      }
    },

    // Search for nearby hotels
    getNearbyPlaces: function (position) {
      axios.get('/api/1.0/hotels/search', {
        params: { ...position }
      })
        .then((response) => {
          this.createMarkers(response.data)
        })
        .catch((error) => {
          console.log(error)
          alert('An error occured')
        })
    },

    haversineDistance: function (pos1, pos2) {
      const lat1 = pos1.lat
      const lon1 = pos1.lng
      const lat2 = pos2.lat
      const lon2 = pos2.lng

      const p = 0.017453292519943295
      const c = Math.cos;
      const a = 0.5 - c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p)) / 2

      return (12742 * Math.asin(Math.sqrt(a))).toFixed(1)
    },

    createMarkers: function (places) {
      this.places = places

      places.forEach((place, index) => {
        let marker = new google.maps.Marker({
          position: place.geometry.location,
          map: this.map,
          title: place.name
        })

        google.maps.event.addListener(marker, 'click', () => {
          this.showHotelDetails = true
          this.selectedPlace = place
          this.placeIndex = index
        })

        // Adjust the map this.bounds to include the location of this marker
        this.bounds.extend(place.geometry.location)
      })
      /* Once all the markers have been placed, adjust the this.bounds of the map to
      * show all the markers within the visible area. */
      this.map.fitBounds(this.bounds)
    },

    startBooking: function (place) {
      this.booking = true
      this.selectedPlace = place
    },

    closeOverlay: function () {
      this.booking = false
      this.selectedPlace = {}
      this.showHotelDetails = false
      this.placeIndex = null
    }
  },

  mounted: function () {
    this.infoWindow = new google.maps.InfoWindow
    this.currentInfoWindow = this.infoWindow
    this.bounds = new google.maps.LatLngBounds()
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: this.currentPosition,
      zoom: 10,
    })
    this.geoLocateUser()
  }
})
