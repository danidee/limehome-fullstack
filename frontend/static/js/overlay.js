Vue.component('overlay', {
  props: {
    visible: Boolean
  },
  
  methods: {
    closeOverlay: function() {
      this.$emit('close-overlay')
    }
  },

  template: `
    <div v-if="visible" class="overlay">
      <div id="overlay-content">
        <span @click="closeOverlay" class="close-button">x</span>
        <slot></slot>
      </div>
    </div>
  `
})