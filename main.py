import os

import uvicorn
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles

from api.models import db, Booking
from api.views import router as api_router


app = FastAPI()
app.mount(
    '/static',
    StaticFiles(directory=os.path.join('frontend', 'static')),
    name='static'
)
app.include_router(api_router, prefix='/api/1.0')


@app.get('/', response_class=HTMLResponse, include_in_schema=False)
def index():
    f = open(os.path.join('frontend', 'index.html'))
    html = f.read()
    f.close()

    return html


# Create database tables
if __name__ == '__main__':
    db.create_tables([Booking])
    port = int(os.environ.get('PORT', 8000))
    uvicorn.run('main:app', host='0.0.0.0', port=port, workers=4)
