import logging

import peewee
from playhouse.db_url import connect


logger = logging.getLogger('peewee')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)
db = connect('sqlite:///database.db')


class BaseModel(peewee.Model):
    class Meta:
        database = db


class Booking(BaseModel):
    id = peewee.AutoField()
    hotel_id = peewee.CharField(max_length=50)
    customer_name = peewee.CharField(max_length=50)
    phone_number = peewee.CharField(max_length=20)
    check_in = peewee.DateField()
    check_out = peewee.DateField()
    number_of_guests = peewee.SmallIntegerField()
