from datetime import date

import pydantic


class BookingSerializer(pydantic.BaseModel):
    class Config:
        orm_mode = True

    hotel_id: pydantic.constr(max_length=50)
    customer_name: pydantic.constr(max_length=50)
    phone_number: pydantic.constr(max_length=20)
    check_in: date
    check_out: date
    number_of_guests: int
