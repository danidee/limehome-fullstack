from fastapi import APIRouter, HTTPException

from .models import Booking
from .settings import Settings
from .serializers import BookingSerializer

import requests
import requests_cache


# Cache requests to speed up/reduce Google API transactions
requests_cache.install_cache('req_cache', backend='memory')
router = APIRouter()


@router.get('/hotels/{hotel_id}/bookings', tags=['hotels'])
async def list_hotel_bookings(hotel_id: str):
    bookings = Booking.select().where(Booking.hotel_id == hotel_id)

    return [BookingSerializer.from_orm(booking) for booking in bookings]


@router.get('/hotels/search', tags=['hotels'])
async def search_hotels(lat: float, lng: float):
    params = {
        'location': f'{lat},{lng}',
        'rankBy': 'distance', 'keyword': 'hotel',
        'radius': 1500, 'type': 'hotel', 'key': Settings.GOOGLE_API_KEY
    }
    response = requests.get(
        'https://maps.googleapis.com/maps/api/place/nearbysearch/json',
        params=params
    )

    if response.status_code != 200:
        raise HTTPException(status_code=400, detail='An error occured')

    return response.json()['results']


@router.post('/bookings', tags=['bookings'])
async def create_booking(booking: BookingSerializer):
    booking_db = Booking.create(**booking.dict())

    return BookingSerializer.from_orm(booking_db)
