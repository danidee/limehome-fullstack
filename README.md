# Description
Since this is a prototype, the goal was to keep things as simple as possible.
so a lot of things have been deliberately ommitted and simplified.

## Frontend (frontend folder)
- Barebones Vue JS with pure JavaScript files (No `.vue` single file components)
- Zero `Node` dependencies, No compilation or build process. Everything runs directly in the browser
- The entry file (i.e index.html) can be served by any capable webserver

## Backend/API (api folder):
- Powered by FastApi
- Hotel data is obtained directly from Google places API

### Data store
- SQLite

### Authentication
- None

### Documentation
- openAPI 3.0 Powered by FastApi
https://limehome-fullstack.herokuapp.com/docs

## Tools and Style Guides
- Python3.9 (PEP-8)
- JavaScript (Standard)

# Setup
Create a new virtual environment`
`python -m venv limehome`

## Activate the virtual environment
`cd limehome; source bin/activate`

## Install the requirements
`pip install -r requirements.txt`

## Start the local server
```bash
export GOOGLE_API_KEY=xxxxx
python main.py
```

The app should accessible via `http://0.0.0.0:8000` or `http://localhost:8000`

## Run api tests
`python -m unittest`

## Tested on
- Chrome 87.0 (Android)
- Chrome 87 (Desktop)
- Firefox 84.0.1 (Desktop)